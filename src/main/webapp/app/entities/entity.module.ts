import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SpringIoRegionMySuffixModule } from './region-my-suffix/region-my-suffix.module';
import { SpringIoCountryMySuffixModule } from './country-my-suffix/country-my-suffix.module';
import { SpringIoLocationMySuffixModule } from './location-my-suffix/location-my-suffix.module';
import { SpringIoDepartmentMySuffixModule } from './department-my-suffix/department-my-suffix.module';
import { SpringIoTaskMySuffixModule } from './task-my-suffix/task-my-suffix.module';
import { SpringIoEmployeeMySuffixModule } from './employee-my-suffix/employee-my-suffix.module';
import { SpringIoJobMySuffixModule } from './job-my-suffix/job-my-suffix.module';
import { SpringIoJobHistoryMySuffixModule } from './job-history-my-suffix/job-history-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        SpringIoRegionMySuffixModule,
        SpringIoCountryMySuffixModule,
        SpringIoLocationMySuffixModule,
        SpringIoDepartmentMySuffixModule,
        SpringIoTaskMySuffixModule,
        SpringIoEmployeeMySuffixModule,
        SpringIoJobMySuffixModule,
        SpringIoJobHistoryMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SpringIoEntityModule {}
